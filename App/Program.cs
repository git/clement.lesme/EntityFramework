﻿using ContextLib;
using Entities;
using Microsoft.Extensions.DependencyModel;
using System.Diagnostics;
using StubbedContextLib;

List<BookEntity> books = new List<BookEntity>();
List<PersonEntity> persons = new List<PersonEntity>();
StubContext myStub = new StubContext();

// Insertion de livres dans la base de données
using (var context = new LibraryContext())
{
    Console.WriteLine("Insertion\n");

    books = await myStub.booksStub();
    persons = await myStub.personsStub();

    // Ajoute chaque person à la base de données de manière asynchrone
    foreach (var person in persons)
    {
        await context.PersonsSet.AddAsync(person);
    }

    await context.SaveChangesAsync();

    // Ajoute chaque livre à la base de données de manière asynchrone
    foreach (var book in books)
    {
        await context.BooksSet.AddAsync(book);
    }

    // Enregistre les modifications dans la base de données
    await context.SaveChangesAsync();
}
/*
// Récupération et affichage des livres de la base de données
using (var context = new LibraryContext())
{
    Console.WriteLine("Récupération\n");

    foreach (var book in context.BooksSet)
    {
        Console.WriteLine($"{book.Title}, Auteur : {book.Author}");
    }
}

// Suppression d'un livre avec l'ID 2 de la base de données
using (var context = new LibraryContext())
{
    Console.WriteLine("Suppression\n");

    var book = context.BooksSet.FirstOrDefault(b => b.Id == 2);
    if (book != null)
    {
        context.BooksSet.Remove(book);
        await context.SaveChangesAsync();
    }
}

// Modification du titre d'un livre avec l'auteur "Martin"
using (var context = new LibraryContext())
{
    Console.WriteLine("Modification\n");

    var bookToUpdate = context.BooksSet.FirstOrDefault(b => b.Author == "Martin");
    if (bookToUpdate != null)
    {
        bookToUpdate.Title = "Soir";
        await context.SaveChangesAsync();
    }
}*/