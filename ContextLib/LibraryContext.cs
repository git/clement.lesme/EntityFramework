﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ContextLib
{
    public class LibraryContext : DbContext
    {
        public DbSet<BookEntity> BooksSet { get; set; }
        public DbSet<PersonEntity> PersonsSet { get; set; }

        public LibraryContext() { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source=Test.Librairy.db");
        }
    }
}

