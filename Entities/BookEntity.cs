﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class BookEntity
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string Isbn { get; set; }

        public long? PersonEntityId { get; set; }

        public PersonEntity? Owner { get; set; } = null!;
    }

}
