# EntityFramework
# BooksEF

EntityFrameworkBooks est un projet utilisant Entity Framework pour gérer une base de données de livres.

## Prérequis

Avant de commencer, assurez-vous d'avoir les éléments suivants installés sur votre machine :

- .NET Core SDK
- Entity Framework Core
- Dotnet version 8.0.0

## Installation

1. Clonez ce dépôt sur votre machine :

  ```bash
  git clone https://codefirst.iut.uca.fr/git/clement.lesme/EntityFramework.git
  ```

2. Accédez au répertoire du projet :

  ```bash
  cd EntityFrameworkBooks
  ```

3. Installer dotnet ef 8.0.0 :
  
  ```bash
  dotnet tool install --global dotnet-ef --version 8.0.0
  ```

4. Appliquez les migrations pour créer la base de données :

  ```bash
  dotnet ef database update
  ```

5. Pour créer les migrations et la base de données :
   
  ```bash
  dotnet ef migrations add ex_01_01_SQLite --context LibraryContext
  dotnet ef database update --context LibraryContext 
  ```
