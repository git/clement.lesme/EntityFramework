﻿using ContextLib;
using Entities;
using Microsoft.Extensions.DependencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StubbedContextLib
{
    public class StubContext : LibraryContext
    {
        public async Task<List<BookEntity>> booksStub()
        {
            List<BookEntity> listBook = new List<BookEntity>();

            //Création des livres
            BookEntity book1 = new BookEntity
            {
                Id = 1,
                Author = "Lechardeur",
                Title = "Comment pécho 100% garanti",
                Isbn = "696969a",
            };

            BookEntity book2 = new BookEntity
            {
                Id = 2,
                Author = "Vianney",
                Title = "Gros",
                Isbn = "0015150",
                PersonEntityId = 1,
            };

            BookEntity book3 = new BookEntity
            {
                Id = 3,
                Author = "Khéna",
                Title = "Khéna; un homme, un livre",
                Isbn = "666666b",
                PersonEntityId = 1,
            };

            //Ajout des livres
            listBook.Add(book1);
            listBook.Add(book2);
            listBook.Add(book3);

            return listBook;
        }

        public async Task<List<PersonEntity>> personsStub()
        {
            List<PersonEntity> ListPerson = new List<PersonEntity>();
            List<BookEntity> l = new List<BookEntity>();

            PersonEntity person1 = new PersonEntity
            {
                Id = 1,
                FirstName = "Khena",
                LastName = "Bruneau",
            };

            ListPerson.Add(person1);

            return ListPerson;
        }
    }
}
